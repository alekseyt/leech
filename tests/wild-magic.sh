#!/bin/sh

[ ! -z "$LEECHT" ] && set "$LEECHT"

. $(dirname $0)/assert.sh

HERE=$(cd "$(dirname "$0")" && pwd)  # current dir
TOOL="$HERE/../sbin/leech-wild-magic"

export DOWNLOADS="$HERE/conf/downloads"
export REVERSE_DOWNLOADS="$HERE/conf/empty-reverse-downloads" # this is here to also test that empty 
                                                              # reverse-matching doesn't block leech
export WILD_DOWNLOADS="$HERE/conf/wild-downloads"

assert "$(echo sup title | $TOOL | wc -l) -eq 0"
assert "$(echo SUP TITLE | $TOOL | wc -l) -eq 0"
assert "$(echo processed.xml [竹] title | $TOOL | wc -l) -eq 1"
assert "$(echo processed.xml [竹] TITLE | $TOOL | wc -l) -eq 1"
assert "$(echo processed.xml [竹] title.txt | $TOOL | wc -l) -eq 1"
assert "$(echo 'file:///something/processed.xml&x=y' [ANOTHER] TITLE | $TOOL | wc -l) -eq 1"
assert "$(echo title.mkv | $TOOL | wc -l) -eq 1"

assert "$(echo ab.cd efgh.mkv | $TOOL | wc -l) -eq 1"  # exact match
assert "$(echo ab cd efgh.mkv | $TOOL | wc -l) -eq 1"  # however, whitespace shouldn't match .
